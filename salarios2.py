import unittest

#definición de la clase empleado:
class Empleado: #Es una plantilla (algo como una fución) para variables y metodos.
    def __init__(self, n, s,i,a): #Le enseña a Python a contruir el emplado o cualquier clase
        self.nombre=n
        self.nomina=s
        self.impuesto=i
        self.antiguedad=a

    def calcular_impuestos(self): #Método (función dentro de la clase)
        impuesto=self.nomina*self.impuesto
        return impuesto
    
    def calcular_coste(self):
        coste_salarial=self.nomina + self.calcular_impuestos() 
        if self.antiguedad>1:
            coste_salarial=coste_salarial-0.1*coste_salarial
        return coste_salarial

    def __str__(self):
        return "El empleado {nombre} debe pagar {pagar:.2f}. \nEl coste salarial de {nombre} es {coste:.2f}.".format(nombre=self.nombre,pagar=self.calcular_impuestos(),coste=self.calcular_coste())
    



