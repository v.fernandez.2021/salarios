Ana={"nombre":"Ana",
    "nomina":30000,
    "impuesto":0.2,
    "antiguedad":2}

Pepe={"nombre":"Pepe",
     "nomina": 40000,
     "impuesto":0.3,
     "antiguedad":1}

María={"nombre":"María",
     "nomina": 25000,
     "impuesto":0.4,
     "antiguedad":3}

Juan={"nombre":"Juan",
     "nomina": 36000,
     "impuesto":0.1,
     "antiguedad":1}

lista_personas=[Ana,Pepe,María,Juan]

def calcular_impuesto(persona):
    impuesto=persona["impuesto"]*persona["nomina"]
    persona.update(pagar=int(impuesto))
    return persona



def calcular_coste(persona):
    coste_salarial=persona["pagar"]+persona["nomina"]
    if persona["antiguedad"]>1:
        coste_salarial=coste_salarial-0.1*coste_salarial
    persona.update(coste=int(coste_salarial))
    return persona

for diccionario in lista_personas:
    diccionario=calcular_impuesto(diccionario)
    diccionario=calcular_coste(diccionario)
    print(diccionario)



