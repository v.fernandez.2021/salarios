import unittest
from salarios2 import Empleado

class TestEmpleado(unittest.TestCase):

    def test_construir(self):
        e1 = Empleado("nombre",5000,0.3,1)
        self.assertEqual(e1.nomina, 5000)

    def test_impuestos(self):
        e1 = Empleado("nombre",5000,0.3,1)
        self.assertEqual(e1.calcular_impuestos(), 1500)
   
    def test_str(self):
        e1 = Empleado("pepe",50000,0.3,1)
        self.assertEqual("El empleado pepe debe pagar 15000.00. \nEl coste salarial de pepe es 65000.00.", e1.__str__())

if __name__ == "__main__":
    unittest.main()